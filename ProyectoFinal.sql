USE [ProyectoFinal]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [int] NOT NULL,
	[Nombre_us] [varchar](50) NULL,
	[account] [varchar](50) NULL,
	[passw] [varchar](50) NULL,
	[Status_admin] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RutinaViernes]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutinaViernes](
	[id_Ejercicio] [int] NOT NULL,
	[Ejercicio_Numero] [nchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Series] [nchar](10) NULL,
	[Repeticiones] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RutinaMiercoles]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutinaMiercoles](
	[id_Ejercicio] [int] NOT NULL,
	[Ejercicio_Numero] [nchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Series] [nchar](10) NULL,
	[Repeticiones] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RutinaMartes]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutinaMartes](
	[id_Ejercicio] [int] NOT NULL,
	[Ejercicio_Numero] [nchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Series] [nchar](10) NULL,
	[Repeticiones] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RutinaLunes]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutinaLunes](
	[id_Ejercicio] [int] NOT NULL,
	[Ejercicio_Numero] [nchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Series] [nchar](10) NULL,
	[Repeticiones] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RutinaJueve]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutinaJueve](
	[id_Ejercicio] [int] NOT NULL,
	[Ejercicio_Numero] [nchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Series] [nchar](10) NULL,
	[Repeticiones] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ropa]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ropa](
	[id_ropa] [int] NOT NULL,
	[Marca] [varchar](50) NULL,
	[Precio] [varchar](50) NULL,
	[Talla] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proteina]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proteina](
	[id_proteina] [int] NOT NULL,
	[Marca] [varchar](50) NULL,
	[Sabor] [varchar](50) NULL,
	[Precio] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facturas]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facturas](
	[NumFac] [int] NULL,
	[FechaFac] [date] NULL,
	[CodCli] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[id_cliente] [int] NOT NULL,
	[NombreCli] [varchar](50) NULL,
	[ApellidosCli] [varchar](50) NULL,
	[Edad] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[ActualizaJueves]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaJueves]

@id_Ejercicio int, @EjercicioNum varchar(100), @nombre varchar(100), @series varchar(100), @rep varchar(100)

as

if not exists (Select id_Ejercicio from RutinaJueves where id_Ejercicio=@id_Ejercicio)
insert into RutinaJueves (id_Ejercicio,Ejercicio_Numero,Nombre,Series,Repeticiones) values (@id_Ejercicio, @EjercicioNum, @nombre, @series, @rep)

else

update RutinaJueves set id_Ejercicio= @id_Ejercicio, Ejercicio_Numero=@EjercicioNum ,Nombre =@nombre,Series=@series ,Repeticiones=@rep where id_Ejercicio= @id_Ejercicio
GO
/****** Object:  Table [dbo].[Detalles]    Script Date: 05/10/2019 13:57:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalles](
	[NumFac] [int] NULL,
	[CodPro] [int] NULL,
	[PrecioVen] [float] NULL,
	[CanVen] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[EliminarJueves]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarJueves]

@id_Ejercicio int

as

delete from RutinaJueves where id_Ejercicio=@id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[EliminarCliente]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarCliente]
@id_cliente int

as

delete from Cliente where id_cliente=@id_cliente
GO
/****** Object:  StoredProcedure [dbo].[DatosFactura]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[DatosFactura] @NumFac int

as


select 
F. *, D.PrecioVen, D.CanVen, C.NombreCli, P.Marca, D.PrecioVen * D.CanVen as importe


from 
Facturas F inner join Detalles D on F.NumFac = D.NumFac
inner join Proteina P on D.CodPro = P.id_proteina
inner join Cliente C on F.CodCli = C.id_cliente

where F.NumFac = @NumFac
GO
/****** Object:  StoredProcedure [dbo].[ActualizaFacturas]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[ActualizaFacturas] @CodCli int

as

declare @NumFac int

select @NumFac = MAX(NumFac) from Facturas

if @NumFac is null set @NumFac =0
set @NumFac = @NumFac+1

insert into Facturas (NumFac, FechaFac, CodCli)values (@NumFac, GETDATE(), @CodCli)

select * from Facturas where NumFac =@NumFac
GO
/****** Object:  StoredProcedure [dbo].[ActualizaDetalles]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaDetalles] @NumFac int, @CodPro int , @PrecioVen float, @CanVen float

as

insert into Detalles (NumFac, CodPro, PrecioVen,CanVen) values (@NumFac, @CodPro, @PrecioVen, @CanVen)
GO
/****** Object:  StoredProcedure [dbo].[ActualizaCliente]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaCliente]
@id_cliente int, @NombreCli varchar(50), @ApellidosCli varchar(50), @Edad varchar(50)

as

--Actualiza proteina

if not EXISTS (SELECT id_cliente FROM Cliente where id_cliente=@id_cliente)
insert into dbo.Cliente (id_cliente,NombreCli,ApellidosCli,Edad)values (@id_cliente,@NombreCli,@ApellidosCli,@Edad)

else

update Cliente set id_cliente=@id_cliente,NombreCli=@NombreCli,ApellidosCli=@ApellidosCli,Edad=@Edad where id_cliente=@id_cliente
GO
/****** Object:  StoredProcedure [dbo].[ActualizaViernes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaViernes]

@id_Ejercicio int, @EjercicioNum varchar(100), @nombre varchar(100), @series varchar(100), @rep varchar(100)

as

if not exists (Select id_Ejercicio from RutinaViernes where id_Ejercicio=@id_Ejercicio)
insert into RutinaViernes (id_Ejercicio,Ejercicio_Numero,Nombre,Series,Repeticiones) values (@id_Ejercicio, @EjercicioNum, @nombre, @series, @rep)

else

update RutinaViernes set id_Ejercicio= @id_Ejercicio, Ejercicio_Numero=@EjercicioNum ,Nombre =@nombre,Series=@series ,Repeticiones=@rep where id_Ejercicio= @id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[ActualizaUsuarios]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaUsuarios]

@id_usu int, @Nombre_us varchar(50), @Account varchar(50), @passw varchar(50), @Status varchar (50)

as

--Actualiza Usuarios

if not EXISTS (SELECT id_usuario FROM Usuarios where id_usuario=@id_usu)
insert into dbo.Usuarios(id_usuario,Nombre_us,account,passw,Status_admin)values (@id_usu, @Nombre_us, @Account, @passw, @Status)

else

update Usuarios set id_usuario=@id_usu,Nombre_us=@Nombre_us,account=@Account,passw=@passw, Status_admin=@Status where id_usuario=@id_usu
GO
/****** Object:  StoredProcedure [dbo].[ActualizaRopa]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaRopa]

@id_ropa int, @Marca varchar(50),@Precio float, @Talla varchar(50)

as

--Actualiza proteina

if not EXISTS (SELECT id_ropa FROM Ropa where id_ropa=@id_ropa)
insert into dbo.Ropa (id_ropa,Marca,Precio,Talla)values (@id_ropa,@Marca,@Precio,@Talla)

else

update Ropa set id_ropa=@id_ropa,Marca=@Marca,Precio=@Precio,Talla=@Talla where id_ropa=@id_ropa
GO
/****** Object:  StoredProcedure [dbo].[ActualizaProteina]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaProteina]

@Id_proteina int, @Marca varchar(50), @Sabor varchar(50),@Precio float

as

--Actualiza proteina

if not EXISTS (SELECT Id_proteina FROM Proteina where id_proteina=@Id_proteina)
insert into Proteina (id_proteina,Marca,Sabor,Precio)values (@Id_proteina,@Marca,@Sabor, @Precio)

else

update Proteina set id_proteina=@Id_proteina,Marca=@Marca ,Sabor= @Sabor,Precio=@Precio where id_proteina=@Id_proteina
GO
/****** Object:  StoredProcedure [dbo].[ActualizaMiercoles]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaMiercoles]

@id_Ejercicio int, @EjercicioNum varchar(100), @nombre varchar(100), @series varchar(100), @rep varchar(100)

as

if not exists (Select id_Ejercicio from RutinaMiercoles where id_Ejercicio=@id_Ejercicio)
insert into RutinaMiercoles (id_Ejercicio,Ejercicio_Numero,Nombre,Series,Repeticiones) values (@id_Ejercicio, @EjercicioNum, @nombre, @series, @rep)

else

update RutinaMiercoles set id_Ejercicio= @id_Ejercicio, Ejercicio_Numero=@EjercicioNum ,Nombre =@nombre,Series=@series ,Repeticiones=@rep where id_Ejercicio= @id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[ActualizaMartes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaMartes]

@id_Ejercicio int, @EjercicioNum varchar(100), @nombre varchar(100), @series varchar(100), @rep varchar(100)

as

if not exists (Select id_Ejercicio from RutinaMartes where id_Ejercicio=@id_Ejercicio)
insert into RutinaMartes (id_Ejercicio,Ejercicio_Numero,Nombre,Series,Repeticiones) values (@id_Ejercicio, @EjercicioNum, @nombre, @series, @rep)

else

update RutinaMartes set id_Ejercicio= @id_Ejercicio, Ejercicio_Numero=@EjercicioNum ,Nombre =@nombre,Series=@series ,Repeticiones=@rep where id_Ejercicio= @id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[ActualizaLunes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ActualizaLunes]

@id_Ejercicio int, @EjercicioNum varchar(100), @nombre varchar(100), @series varchar(100), @rep varchar(100)

as

if not exists (Select id_Ejercicio from RutinaLunes where id_Ejercicio=@id_Ejercicio)
insert into RutinaLunes (id_Ejercicio,Ejercicio_Numero,Nombre,Series,Repeticiones) values (@id_Ejercicio, @EjercicioNum, @nombre, @series, @rep)

else

update RutinaLunes set id_Ejercicio= @id_Ejercicio, Ejercicio_Numero=@EjercicioNum ,Nombre =@nombre,Series=@series ,Repeticiones=@rep where id_Ejercicio= @id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[EliminaUsuarios]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminaUsuarios]

@id_usu int

as

delete from Usuarios where id_usuario = @id_usu
GO
/****** Object:  StoredProcedure [dbo].[EliminarViernes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarViernes]

@id_Ejercicio int

as

delete from RutinaViernes where id_Ejercicio=@id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[EliminarRopa]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarRopa]
@id_ropa int

as

delete from Ropa where id_ropa=@id_ropa
GO
/****** Object:  StoredProcedure [dbo].[EliminarProteina]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarProteina]
@id_proteina int

as

delete from Proteina where id_proteina=@id_proteina
GO
/****** Object:  StoredProcedure [dbo].[EliminarMiercoles]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarMiercoles]

@id_Ejercicio int

as

delete from RutinaMiercoles where id_Ejercicio=@id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[EliminarMartes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarMartes]

@id_Ejercicio int

as

delete from RutinaMartes where id_Ejercicio=@id_Ejercicio
GO
/****** Object:  StoredProcedure [dbo].[EliminarLunes]    Script Date: 05/10/2019 13:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[EliminarLunes]

@id_Ejercicio int

as

delete from RutinaLunes where id_Ejercicio=@id_Ejercicio
GO
