﻿namespace ProyectoFinal
{
    partial class GestionRopa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSignoP = new System.Windows.Forms.Label();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.lblTalla = new System.Windows.Forms.Label();
            this.lblId_Ropa = new System.Windows.Forms.Label();
            this.txtPrecio = new MiLibreria.ErrortxtBox();
            this.txtTalla = new MiLibreria.ErrortxtBox();
            this.txtMarca = new MiLibreria.ErrortxtBox();
            this.txtIdRopa = new MiLibreria.ErrortxtBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(395, 199);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(395, 250);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(395, 299);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(395, 344);
            // 
            // lblSignoP
            // 
            this.lblSignoP.AutoSize = true;
            this.lblSignoP.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignoP.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSignoP.Location = new System.Drawing.Point(159, 344);
            this.lblSignoP.Name = "lblSignoP";
            this.lblSignoP.Size = new System.Drawing.Size(15, 18);
            this.lblSignoP.TabIndex = 24;
            this.lblSignoP.Text = "$";
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPrecio.Location = new System.Drawing.Point(64, 341);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(46, 18);
            this.lblPrecio.TabIndex = 22;
            this.lblPrecio.Text = "Precio";
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarca.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMarca.Location = new System.Drawing.Point(64, 247);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(46, 18);
            this.lblMarca.TabIndex = 20;
            this.lblMarca.Text = "Marca";
            // 
            // lblTalla
            // 
            this.lblTalla.AutoSize = true;
            this.lblTalla.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTalla.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTalla.Location = new System.Drawing.Point(64, 294);
            this.lblTalla.Name = "lblTalla";
            this.lblTalla.Size = new System.Drawing.Size(38, 18);
            this.lblTalla.TabIndex = 19;
            this.lblTalla.Text = "Talla";
            // 
            // lblId_Ropa
            // 
            this.lblId_Ropa.AutoSize = true;
            this.lblId_Ropa.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId_Ropa.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblId_Ropa.Location = new System.Drawing.Point(64, 200);
            this.lblId_Ropa.Name = "lblId_Ropa";
            this.lblId_Ropa.Size = new System.Drawing.Size(60, 18);
            this.lblId_Ropa.TabIndex = 18;
            this.lblId_Ropa.Text = "Id_Ropa";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(180, 342);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(153, 20);
            this.txtPrecio.SoloN = false;
            this.txtPrecio.TabIndex = 28;
            this.txtPrecio.Validar = true;
            // 
            // txtTalla
            // 
            this.txtTalla.Location = new System.Drawing.Point(149, 292);
            this.txtTalla.Name = "txtTalla";
            this.txtTalla.Size = new System.Drawing.Size(184, 20);
            this.txtTalla.SoloN = false;
            this.txtTalla.TabIndex = 29;
            this.txtTalla.Validar = true;
            // 
            // txtMarca
            // 
            this.txtMarca.Location = new System.Drawing.Point(149, 247);
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(184, 20);
            this.txtMarca.SoloN = false;
            this.txtMarca.TabIndex = 30;
            this.txtMarca.Validar = true;
            // 
            // txtIdRopa
            // 
            this.txtIdRopa.Location = new System.Drawing.Point(149, 202);
            this.txtIdRopa.Name = "txtIdRopa";
            this.txtIdRopa.Size = new System.Drawing.Size(184, 20);
            this.txtIdRopa.SoloN = true;
            this.txtIdRopa.TabIndex = 31;
            this.txtIdRopa.Validar = true;
            this.txtIdRopa.TextChanged += new System.EventHandler(this.txtIdRopa_TextChanged);
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ProyectoFinal.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(149, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 171);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 32;
            this.picLogo.TabStop = false;
            // 
            // GestionRopa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 411);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.txtIdRopa);
            this.Controls.Add(this.txtMarca);
            this.Controls.Add(this.txtTalla);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.lblSignoP);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.lblMarca);
            this.Controls.Add(this.lblTalla);
            this.Controls.Add(this.lblId_Ropa);
            this.Name = "GestionRopa";
            this.Text = "GestionRopa";
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblId_Ropa, 0);
            this.Controls.SetChildIndex(this.lblTalla, 0);
            this.Controls.SetChildIndex(this.lblMarca, 0);
            this.Controls.SetChildIndex(this.lblPrecio, 0);
            this.Controls.SetChildIndex(this.lblSignoP, 0);
            this.Controls.SetChildIndex(this.txtPrecio, 0);
            this.Controls.SetChildIndex(this.txtTalla, 0);
            this.Controls.SetChildIndex(this.txtMarca, 0);
            this.Controls.SetChildIndex(this.txtIdRopa, 0);
            this.Controls.SetChildIndex(this.picLogo, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSignoP;
        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.Label lblTalla;
        private System.Windows.Forms.Label lblId_Ropa;
        private MiLibreria.ErrortxtBox txtPrecio;
        private MiLibreria.ErrortxtBox txtTalla;
        private MiLibreria.ErrortxtBox txtMarca;
        private MiLibreria.ErrortxtBox txtIdRopa;
        private System.Windows.Forms.PictureBox picLogo;
    }
}