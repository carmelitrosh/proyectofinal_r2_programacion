﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class Contenedor : Form
    {
        private int childFormNumber = 0;

        public Contenedor()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }



        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionCliente MainCli = new GestionCliente();
            MainCli.MdiParent = this;
            MainCli.Show();
        }

        private void proteinaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionProteina MainCli = new GestionProteina();
            MainCli.MdiParent = this;
            MainCli.Show();
        }

        private void ropaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionRopa MainCli = new GestionRopa();
            MainCli.MdiParent = this;
            MainCli.Show();
        }

        private void clienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarCliente ConCli = new ConsultarCliente();
            ConCli.MdiParent = this;
            ConCli.Show();
        }

        private void proteinaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultaProteina ConPro = new ConsultaProteina();
            ConPro.MdiParent = this;
            ConPro.Show();
        }

        private void ropaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultaRopa ConR = new ConsultaRopa();
            ConR.MdiParent = this;
            ConR.Show();
        }

        private void facturacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Factura fac = new Factura();
            fac.MdiParent = this;
            fac.Show();
        }

        private void rutinaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rutina Rut = new Rutina();
            Rut.MdiParent = this;
            Rut.Show();
        }


        private void lunesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            GestionRutinaLunes GR= new GestionRutinaLunes();
            GR.MdiParent = this;
            GR.Show();
        }

        private void martesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionRutinaMartes GR = new GestionRutinaMartes();
            GR.MdiParent = this;
            GR.Show();
        }

        private void miercolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionRutinaMiercoles GR = new GestionRutinaMiercoles();
            GR.MdiParent = this;
            GR.Show();
        }

        private void juevesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionRutinaJueves GR = new GestionRutinaJueves();
            GR.MdiParent = this;
            GR.Show();
        }

        private void viernesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionRutinaViernes GR = new GestionRutinaViernes();
            GR.MdiParent = this;
            GR.Show();
        }

        private void facturacionToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Factura GR = new Factura();
            GR.MdiParent = this;
            GR.Show();
        }
    }
}
