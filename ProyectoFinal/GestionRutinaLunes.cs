﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class GestionRutinaLunes : Gestion
    {
        public GestionRutinaLunes()
        {
            InitializeComponent();
        }


        public override Boolean Guardar()
        {
            if (Utilidades.ValidarFormulario(this, errorProvider1) == false)
            {
                try
                {
                    string cmd = string.Format("EXEC ActualizaLunes '{0}','{1}','{2}','{3}', '{4}'", txtIdEjercicio.Text.Trim(), txtNumEjercicio.Text.Trim(), txtNombre.Text.Trim(), txtSeries.Text.Trim(), txtRepeticiones.Text.Trim());
                    Utilidades.Ejecutar(cmd);

                    MessageBox.Show("Se ha guardado correctamente");
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarLunes '{0}'", txtIdEjercicio.Text.Trim());
                Utilidades.Ejecutar(cmd);

                MessageBox.Show("Se ha eliminado");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);

            }
        }


    }
}
