﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class GestionProteina : Gestion
    {
        public GestionProteina()
        {
            InitializeComponent();
        }

        public override Boolean Guardar()
        {
            if(Utilidades.ValidarFormulario(this, errorProvider1) == false)
            {
                try
                {
                    string cmd = string.Format("EXEC ActualizaProteina '{0}','{1}','{2}','{3}'", txtIdProteina.Text.Trim(), txtMarca.Text.Trim(), txtSabor.Text.Trim(), txtPrecio.Text.Trim());
                    Utilidades.Ejecutar(cmd);

                    MessageBox.Show("Se ha guardado correctamente");
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarProteina '{0}'", txtIdProteina.Text.Trim());
                Utilidades.Ejecutar(cmd);

                MessageBox.Show("Se ha eliminado");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);

            }
        }

        private void txtIdProteina_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void lblId_Proteina_Click(object sender, EventArgs e)
        {

        }
    }
}
