﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class AdministracionUsu : Gestion
    {
        public AdministracionUsu()
        {
            InitializeComponent();
        }

        private void AdministracionUsu_Load(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }




        public override Boolean Guardar()
        {
            if (Utilidades.ValidarFormulario(this, errorProvider1) == false)
            {
                try
                {
                    string cmd = string.Format("EXEC ActualizaCliente '{0}','{1}','{2}','{3}', {4}", txtIdUsu.Text.Trim(), txtNombre.Text.Trim(), txtUsu.Text.Trim(), txtContra.Text.Trim(), txtStatus.Text.Trim());
                    Utilidades.Ejecutar(cmd);

                    MessageBox.Show("Se ha guardado correctamente");
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error" + error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarCliente '{0}'", txtIdUsu.Text.Trim());
                Utilidades.Ejecutar(cmd);

                MessageBox.Show("Se ha eliminado");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);

            }
        }

        private void txtIdRopa_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {

        }
    }
}
