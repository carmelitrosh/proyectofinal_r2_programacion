﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class ConsultaRopa : Consultas
    {
        public ConsultaRopa()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds;

                string cmd = "Select * from Ropa where Marca LIKE ('%" + textBox1.Text.Trim() + "%')";

                ds = Utilidades.Ejecutar(cmd);

                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }
        }

        private void ConsultaRopa_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = llenarDataGV("Ropa").Tables[0];
        }
    }
}
