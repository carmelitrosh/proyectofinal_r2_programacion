﻿namespace ProyectoFinal
{
    partial class AdministracionUsu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombre = new MiLibreria.ErrortxtBox();
            this.txtUsu = new MiLibreria.ErrortxtBox();
            this.txtContra = new MiLibreria.ErrortxtBox();
            this.txtIdUsu = new MiLibreria.ErrortxtBox();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblId_Cliente = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.txtStatus = new MiLibreria.ErrortxtBox();
            this.lblStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(217, 357);
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(353, 357);
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(478, 357);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(612, 357);
            this.btnSalir.Size = new System.Drawing.Size(84, 22);
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(414, 153);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(282, 20);
            this.txtNombre.SoloN = false;
            this.txtNombre.TabIndex = 35;
            this.txtNombre.Validar = true;
            // 
            // txtUsu
            // 
            this.txtUsu.Location = new System.Drawing.Point(414, 201);
            this.txtUsu.Name = "txtUsu";
            this.txtUsu.Size = new System.Drawing.Size(282, 20);
            this.txtUsu.SoloN = false;
            this.txtUsu.TabIndex = 34;
            this.txtUsu.Validar = true;
            // 
            // txtContra
            // 
            this.txtContra.Location = new System.Drawing.Point(414, 244);
            this.txtContra.Name = "txtContra";
            this.txtContra.Size = new System.Drawing.Size(282, 20);
            this.txtContra.SoloN = true;
            this.txtContra.TabIndex = 33;
            this.txtContra.Validar = true;
            // 
            // txtIdUsu
            // 
            this.txtIdUsu.Location = new System.Drawing.Point(414, 105);
            this.txtIdUsu.Name = "txtIdUsu";
            this.txtIdUsu.Size = new System.Drawing.Size(282, 20);
            this.txtIdUsu.SoloN = true;
            this.txtIdUsu.TabIndex = 32;
            this.txtIdUsu.Validar = true;
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblEdad.Location = new System.Drawing.Point(313, 246);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(95, 22);
            this.lblEdad.TabIndex = 31;
            this.lblEdad.Text = "Contraseña";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNombre.Location = new System.Drawing.Point(313, 152);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(72, 22);
            this.lblNombre.TabIndex = 30;
            this.lblNombre.Text = "Nombre";
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblApellidos.Location = new System.Drawing.Point(313, 199);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(68, 22);
            this.lblApellidos.TabIndex = 29;
            this.lblApellidos.Text = "Usuario";
            // 
            // lblId_Cliente
            // 
            this.lblId_Cliente.AutoSize = true;
            this.lblId_Cliente.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId_Cliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblId_Cliente.Location = new System.Drawing.Point(313, 105);
            this.lblId_Cliente.Name = "lblId_Cliente";
            this.lblId_Cliente.Size = new System.Drawing.Size(92, 22);
            this.lblId_Cliente.TabIndex = 28;
            this.lblId_Cliente.Text = "Id_Usuario";
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ProyectoFinal.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(62, 93);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 171);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 36;
            this.picLogo.TabStop = false;
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(414, 284);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(282, 20);
            this.txtStatus.SoloN = true;
            this.txtStatus.TabIndex = 38;
            this.txtStatus.Validar = true;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblStatus.Location = new System.Drawing.Point(313, 286);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(64, 22);
            this.lblStatus.TabIndex = 37;
            this.lblStatus.Text = "Status*";
            // 
            // AdministracionUsu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 529);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtUsu);
            this.Controls.Add(this.txtContra);
            this.Controls.Add(this.txtIdUsu);
            this.Controls.Add(this.lblEdad);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.lblId_Cliente);
            this.Name = "AdministracionUsu";
            this.Text = "AdministracionUsu";
            this.Load += new System.EventHandler(this.AdministracionUsu_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblId_Cliente, 0);
            this.Controls.SetChildIndex(this.lblApellidos, 0);
            this.Controls.SetChildIndex(this.lblNombre, 0);
            this.Controls.SetChildIndex(this.lblEdad, 0);
            this.Controls.SetChildIndex(this.txtIdUsu, 0);
            this.Controls.SetChildIndex(this.txtContra, 0);
            this.Controls.SetChildIndex(this.txtUsu, 0);
            this.Controls.SetChildIndex(this.txtNombre, 0);
            this.Controls.SetChildIndex(this.picLogo, 0);
            this.Controls.SetChildIndex(this.lblStatus, 0);
            this.Controls.SetChildIndex(this.txtStatus, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MiLibreria.ErrortxtBox txtNombre;
        private MiLibreria.ErrortxtBox txtUsu;
        private MiLibreria.ErrortxtBox txtContra;
        private MiLibreria.ErrortxtBox txtIdUsu;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label lblId_Cliente;
        private System.Windows.Forms.PictureBox picLogo;
        private MiLibreria.ErrortxtBox txtStatus;
        private System.Windows.Forms.Label lblStatus;
    }
}