﻿namespace ProyectoFinal
{
    partial class Rutina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.checkBoxLunes = new System.Windows.Forms.CheckBox();
            this.checkBoxViernes = new System.Windows.Forms.CheckBox();
            this.checkBoxJueves = new System.Windows.Forms.CheckBox();
            this.checkBoxMiercoles = new System.Windows.Forms.CheckBox();
            this.checkBoxMartes = new System.Windows.Forms.CheckBox();
            this.btnRutinaDia = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Size = new System.Drawing.Size(10, 23);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Size = new System.Drawing.Size(10, 23);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Size = new System.Drawing.Size(10, 23);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(87, 354);
            this.btnSalir.Size = new System.Drawing.Size(230, 23);
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ProyectoFinal.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(87, 50);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 171);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 7;
            this.picLogo.TabStop = false;
            // 
            // checkBoxLunes
            // 
            this.checkBoxLunes.AutoSize = true;
            this.checkBoxLunes.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxLunes.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLunes.ForeColor = System.Drawing.Color.White;
            this.checkBoxLunes.Location = new System.Drawing.Point(413, 50);
            this.checkBoxLunes.Name = "checkBoxLunes";
            this.checkBoxLunes.Size = new System.Drawing.Size(149, 48);
            this.checkBoxLunes.TabIndex = 8;
            this.checkBoxLunes.Text = "LUNES";
            this.checkBoxLunes.UseVisualStyleBackColor = false;
            this.checkBoxLunes.CheckedChanged += new System.EventHandler(this.checkBoxLunes_CheckedChanged);
            // 
            // checkBoxViernes
            // 
            this.checkBoxViernes.AutoSize = true;
            this.checkBoxViernes.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxViernes.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxViernes.ForeColor = System.Drawing.Color.White;
            this.checkBoxViernes.Location = new System.Drawing.Point(413, 364);
            this.checkBoxViernes.Name = "checkBoxViernes";
            this.checkBoxViernes.Size = new System.Drawing.Size(184, 48);
            this.checkBoxViernes.TabIndex = 9;
            this.checkBoxViernes.Text = "VIERNES";
            this.checkBoxViernes.UseVisualStyleBackColor = false;
            // 
            // checkBoxJueves
            // 
            this.checkBoxJueves.AutoSize = true;
            this.checkBoxJueves.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxJueves.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxJueves.ForeColor = System.Drawing.Color.White;
            this.checkBoxJueves.Location = new System.Drawing.Point(413, 287);
            this.checkBoxJueves.Name = "checkBoxJueves";
            this.checkBoxJueves.Size = new System.Drawing.Size(159, 48);
            this.checkBoxJueves.TabIndex = 10;
            this.checkBoxJueves.Text = "JUEVES";
            this.checkBoxJueves.UseVisualStyleBackColor = false;
            // 
            // checkBoxMiercoles
            // 
            this.checkBoxMiercoles.AutoSize = true;
            this.checkBoxMiercoles.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxMiercoles.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMiercoles.ForeColor = System.Drawing.Color.White;
            this.checkBoxMiercoles.Location = new System.Drawing.Point(413, 207);
            this.checkBoxMiercoles.Name = "checkBoxMiercoles";
            this.checkBoxMiercoles.Size = new System.Drawing.Size(233, 48);
            this.checkBoxMiercoles.TabIndex = 11;
            this.checkBoxMiercoles.Text = "MIERCOLES";
            this.checkBoxMiercoles.UseVisualStyleBackColor = false;
            // 
            // checkBoxMartes
            // 
            this.checkBoxMartes.AutoSize = true;
            this.checkBoxMartes.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxMartes.Font = new System.Drawing.Font("Palatino Linotype", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMartes.ForeColor = System.Drawing.Color.White;
            this.checkBoxMartes.Location = new System.Drawing.Point(413, 127);
            this.checkBoxMartes.Name = "checkBoxMartes";
            this.checkBoxMartes.Size = new System.Drawing.Size(179, 48);
            this.checkBoxMartes.TabIndex = 12;
            this.checkBoxMartes.Text = "MARTES";
            this.checkBoxMartes.UseVisualStyleBackColor = false;
            // 
            // btnRutinaDia
            // 
            this.btnRutinaDia.Location = new System.Drawing.Point(87, 287);
            this.btnRutinaDia.Name = "btnRutinaDia";
            this.btnRutinaDia.Size = new System.Drawing.Size(230, 23);
            this.btnRutinaDia.TabIndex = 13;
            this.btnRutinaDia.Text = "Rutina del dia";
            this.btnRutinaDia.UseVisualStyleBackColor = true;
            this.btnRutinaDia.Click += new System.EventHandler(this.btnRutinaDia_Click);
            // 
            // Rutina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnRutinaDia);
            this.Controls.Add(this.checkBoxMartes);
            this.Controls.Add(this.checkBoxMiercoles);
            this.Controls.Add(this.checkBoxJueves);
            this.Controls.Add(this.checkBoxViernes);
            this.Controls.Add(this.checkBoxLunes);
            this.Controls.Add(this.picLogo);
            this.Name = "Rutina";
            this.Text = "Rutina";
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.picLogo, 0);
            this.Controls.SetChildIndex(this.checkBoxLunes, 0);
            this.Controls.SetChildIndex(this.checkBoxViernes, 0);
            this.Controls.SetChildIndex(this.checkBoxJueves, 0);
            this.Controls.SetChildIndex(this.checkBoxMiercoles, 0);
            this.Controls.SetChildIndex(this.checkBoxMartes, 0);
            this.Controls.SetChildIndex(this.btnRutinaDia, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.CheckBox checkBoxLunes;
        private System.Windows.Forms.CheckBox checkBoxViernes;
        private System.Windows.Forms.CheckBox checkBoxJueves;
        private System.Windows.Forms.CheckBox checkBoxMiercoles;
        private System.Windows.Forms.CheckBox checkBoxMartes;
        private System.Windows.Forms.Button btnRutinaDia;
    }
}