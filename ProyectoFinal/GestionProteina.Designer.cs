﻿namespace ProyectoFinal
{
    partial class GestionProteina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblId_Proteina = new System.Windows.Forms.Label();
            this.lblSabor = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.lblSignoP = new System.Windows.Forms.Label();
            this.txtPrecio = new MiLibreria.ErrortxtBox();
            this.txtSabor = new MiLibreria.ErrortxtBox();
            this.txtIdProteina = new MiLibreria.ErrortxtBox();
            this.txtMarca = new MiLibreria.ErrortxtBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(395, 191);
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(395, 242);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(395, 291);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(395, 336);
            // 
            // lblId_Proteina
            // 
            this.lblId_Proteina.AutoSize = true;
            this.lblId_Proteina.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId_Proteina.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblId_Proteina.Location = new System.Drawing.Point(73, 197);
            this.lblId_Proteina.Name = "lblId_Proteina";
            this.lblId_Proteina.Size = new System.Drawing.Size(79, 18);
            this.lblId_Proteina.TabIndex = 8;
            this.lblId_Proteina.Text = "Id_Proteina";
            this.lblId_Proteina.Click += new System.EventHandler(this.lblId_Proteina_Click);
            // 
            // lblSabor
            // 
            this.lblSabor.AutoSize = true;
            this.lblSabor.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSabor.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSabor.Location = new System.Drawing.Point(73, 291);
            this.lblSabor.Name = "lblSabor";
            this.lblSabor.Size = new System.Drawing.Size(44, 18);
            this.lblSabor.TabIndex = 9;
            this.lblSabor.Text = "Sabor";
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarca.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMarca.Location = new System.Drawing.Point(73, 244);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(46, 18);
            this.lblMarca.TabIndex = 10;
            this.lblMarca.Text = "Marca";
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblPrecio.Location = new System.Drawing.Point(73, 338);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(46, 18);
            this.lblPrecio.TabIndex = 12;
            this.lblPrecio.Text = "Precio";
            // 
            // lblSignoP
            // 
            this.lblSignoP.AutoSize = true;
            this.lblSignoP.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignoP.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSignoP.Location = new System.Drawing.Point(168, 341);
            this.lblSignoP.Name = "lblSignoP";
            this.lblSignoP.Size = new System.Drawing.Size(15, 18);
            this.lblSignoP.TabIndex = 14;
            this.lblSignoP.Text = "$";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(189, 336);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(153, 20);
            this.txtPrecio.SoloN = false;
            this.txtPrecio.TabIndex = 25;
            this.txtPrecio.Validar = true;
            // 
            // txtSabor
            // 
            this.txtSabor.Location = new System.Drawing.Point(158, 291);
            this.txtSabor.Name = "txtSabor";
            this.txtSabor.Size = new System.Drawing.Size(184, 20);
            this.txtSabor.SoloN = false;
            this.txtSabor.TabIndex = 26;
            this.txtSabor.Validar = true;
            // 
            // txtIdProteina
            // 
            this.txtIdProteina.Location = new System.Drawing.Point(158, 197);
            this.txtIdProteina.Name = "txtIdProteina";
            this.txtIdProteina.Size = new System.Drawing.Size(184, 20);
            this.txtIdProteina.SoloN = true;
            this.txtIdProteina.TabIndex = 27;
            this.txtIdProteina.Validar = true;
            this.txtIdProteina.TextChanged += new System.EventHandler(this.txtIdProteina_TextChanged);
            // 
            // txtMarca
            // 
            this.txtMarca.Location = new System.Drawing.Point(158, 245);
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(184, 20);
            this.txtMarca.SoloN = false;
            this.txtMarca.TabIndex = 28;
            this.txtMarca.Validar = true;
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ProyectoFinal.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(158, 1);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 171);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 29;
            this.picLogo.TabStop = false;
            // 
            // GestionProteina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 411);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.txtMarca);
            this.Controls.Add(this.txtIdProteina);
            this.Controls.Add(this.txtSabor);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.lblSignoP);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.lblMarca);
            this.Controls.Add(this.lblSabor);
            this.Controls.Add(this.lblId_Proteina);
            this.Name = "GestionProteina";
            this.Text = "GestionProteina";
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblId_Proteina, 0);
            this.Controls.SetChildIndex(this.lblSabor, 0);
            this.Controls.SetChildIndex(this.lblMarca, 0);
            this.Controls.SetChildIndex(this.lblPrecio, 0);
            this.Controls.SetChildIndex(this.lblSignoP, 0);
            this.Controls.SetChildIndex(this.txtPrecio, 0);
            this.Controls.SetChildIndex(this.txtSabor, 0);
            this.Controls.SetChildIndex(this.txtIdProteina, 0);
            this.Controls.SetChildIndex(this.txtMarca, 0);
            this.Controls.SetChildIndex(this.picLogo, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblId_Proteina;
        private System.Windows.Forms.Label lblSabor;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.Label lblSignoP;
        private MiLibreria.ErrortxtBox txtPrecio;
        private MiLibreria.ErrortxtBox txtSabor;
        private MiLibreria.ErrortxtBox txtIdProteina;
        private MiLibreria.ErrortxtBox txtMarca;
        private System.Windows.Forms.PictureBox picLogo;
    }
}