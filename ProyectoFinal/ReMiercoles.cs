﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class ReMiercoles : Consultas
    {
        public ReMiercoles()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds;

                string cmd = "Select * from RutinaMiercoles where Nombre LIKE ('%" + textBox1.Text.Trim() + "%')";

                ds = Utilidades.Ejecutar(cmd);

                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (checkBoxEje1.Checked == true && checkBoxEje2.Checked == true && checkBoxEje3.Checked == true && checkBoxEje4.Checked == true && checkBoxEje5.Checked == true)
            {
                MessageBox.Show("Completaste el dia MIERCOLES con exito, Pasaras al dia siguiente");

                ReJueves RDia = new ReJueves();
                this.Hide();
                RDia.Show();
            }
            else
            {
                MessageBox.Show("Te faltan ejercicios por cumplir");
            }
        }

        private void ReMiercoles_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = llenarDataGV("RutinaMiercoles").Tables[0];
        }
    }
}
