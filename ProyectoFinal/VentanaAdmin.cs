﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class VentanaAdmin : FormBase
    {
        public VentanaAdmin()
        {
            InitializeComponent();
        }

        private void VentanaAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void VentanaAdmin_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuarios WHERE id_usuario=" + INICIO.Codigo;

            DataSet DS = Utilidades.Ejecutar(cmd);

            lblNomAd.Text = DS.Tables[0].Rows[0]["Nombre_us"].ToString();
            lblUsAdmin.Text = DS.Tables[0].Rows[0]["account"].ToString();
            lblCodigo.Text = DS.Tables[0].Rows[0]["id_usuario"].ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Contenedor conP = new Contenedor();
            this.Hide();
            conP.Show();
        }

        private void btnCambiar_Click(object sender, EventArgs e)
        {
            INICIO conP = new INICIO();
            this.Hide();
            conP.Show();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            INICIO conP = new INICIO();
            this.Hide();
            conP.Show();
        }

        private void btnAdministrar_Click(object sender, EventArgs e)
        {
            AdministracionUsu conP = new AdministracionUsu();
            this.Hide();
            conP.Show();
        }
    }
}
