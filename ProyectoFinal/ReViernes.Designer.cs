﻿namespace ProyectoFinal
{
    partial class ReViernes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDiaSemana = new System.Windows.Forms.Label();
            this.checkBoxEje6 = new System.Windows.Forms.CheckBox();
            this.checkBoxEje2 = new System.Windows.Forms.CheckBox();
            this.checkBoxEje3 = new System.Windows.Forms.CheckBox();
            this.checkBoxEje4 = new System.Windows.Forms.CheckBox();
            this.checkBoxEje5 = new System.Windows.Forms.CheckBox();
            this.checkBoxEje1 = new System.Windows.Forms.CheckBox();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBuscar
            // 
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Location = new System.Drawing.Point(404, 22);
            this.btnSeleccionar.Size = new System.Drawing.Size(10, 23);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(404, 22);
            this.btnImprimir.Size = new System.Drawing.Size(10, 23);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(12, 525);
            this.btnSalir.Size = new System.Drawing.Size(190, 25);
            // 
            // lblDiaSemana
            // 
            this.lblDiaSemana.AutoSize = true;
            this.lblDiaSemana.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaSemana.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDiaSemana.Location = new System.Drawing.Point(769, 15);
            this.lblDiaSemana.Name = "lblDiaSemana";
            this.lblDiaSemana.Size = new System.Drawing.Size(87, 28);
            this.lblDiaSemana.TabIndex = 27;
            this.lblDiaSemana.Text = "Viernes";
            // 
            // checkBoxEje6
            // 
            this.checkBoxEje6.AutoSize = true;
            this.checkBoxEje6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje6.Location = new System.Drawing.Point(783, 366);
            this.checkBoxEje6.Name = "checkBoxEje6";
            this.checkBoxEje6.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje6.TabIndex = 26;
            this.checkBoxEje6.Text = "Ejercicio 6";
            this.checkBoxEje6.UseVisualStyleBackColor = false;
            // 
            // checkBoxEje2
            // 
            this.checkBoxEje2.AutoSize = true;
            this.checkBoxEje2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje2.Location = new System.Drawing.Point(783, 117);
            this.checkBoxEje2.Name = "checkBoxEje2";
            this.checkBoxEje2.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje2.TabIndex = 25;
            this.checkBoxEje2.Text = "Ejercicio 2";
            this.checkBoxEje2.UseVisualStyleBackColor = false;
            // 
            // checkBoxEje3
            // 
            this.checkBoxEje3.AutoSize = true;
            this.checkBoxEje3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje3.Location = new System.Drawing.Point(783, 181);
            this.checkBoxEje3.Name = "checkBoxEje3";
            this.checkBoxEje3.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje3.TabIndex = 24;
            this.checkBoxEje3.Text = "Ejercicio 3";
            this.checkBoxEje3.UseVisualStyleBackColor = false;
            // 
            // checkBoxEje4
            // 
            this.checkBoxEje4.AutoSize = true;
            this.checkBoxEje4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje4.Location = new System.Drawing.Point(783, 238);
            this.checkBoxEje4.Name = "checkBoxEje4";
            this.checkBoxEje4.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje4.TabIndex = 23;
            this.checkBoxEje4.Text = "Ejercicio 4";
            this.checkBoxEje4.UseVisualStyleBackColor = false;
            // 
            // checkBoxEje5
            // 
            this.checkBoxEje5.AutoSize = true;
            this.checkBoxEje5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje5.Location = new System.Drawing.Point(783, 302);
            this.checkBoxEje5.Name = "checkBoxEje5";
            this.checkBoxEje5.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje5.TabIndex = 22;
            this.checkBoxEje5.Text = "Ejercicio 5";
            this.checkBoxEje5.UseVisualStyleBackColor = false;
            // 
            // checkBoxEje1
            // 
            this.checkBoxEje1.AutoSize = true;
            this.checkBoxEje1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.checkBoxEje1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.checkBoxEje1.Location = new System.Drawing.Point(783, 55);
            this.checkBoxEje1.Name = "checkBoxEje1";
            this.checkBoxEje1.Size = new System.Drawing.Size(75, 17);
            this.checkBoxEje1.TabIndex = 21;
            this.checkBoxEje1.Text = "Ejercicio 1";
            this.checkBoxEje1.UseVisualStyleBackColor = false;
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Location = new System.Drawing.Point(587, 525);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(190, 25);
            this.btnSiguiente.TabIndex = 28;
            this.btnSiguiente.Text = "Continuar";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // ReViernes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 576);
            this.Controls.Add(this.btnSiguiente);
            this.Controls.Add(this.lblDiaSemana);
            this.Controls.Add(this.checkBoxEje6);
            this.Controls.Add(this.checkBoxEje2);
            this.Controls.Add(this.checkBoxEje3);
            this.Controls.Add(this.checkBoxEje4);
            this.Controls.Add(this.checkBoxEje5);
            this.Controls.Add(this.checkBoxEje1);
            this.Name = "ReViernes";
            this.Text = "ReViernes";
            this.Load += new System.EventHandler(this.ReViernes_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnSeleccionar, 0);
            this.Controls.SetChildIndex(this.btnImprimir, 0);
            this.Controls.SetChildIndex(this.textBox1, 0);
            this.Controls.SetChildIndex(this.btnBuscar, 0);
            this.Controls.SetChildIndex(this.checkBoxEje1, 0);
            this.Controls.SetChildIndex(this.checkBoxEje5, 0);
            this.Controls.SetChildIndex(this.checkBoxEje4, 0);
            this.Controls.SetChildIndex(this.checkBoxEje3, 0);
            this.Controls.SetChildIndex(this.checkBoxEje2, 0);
            this.Controls.SetChildIndex(this.checkBoxEje6, 0);
            this.Controls.SetChildIndex(this.lblDiaSemana, 0);
            this.Controls.SetChildIndex(this.btnSiguiente, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDiaSemana;
        public System.Windows.Forms.CheckBox checkBoxEje6;
        public System.Windows.Forms.CheckBox checkBoxEje2;
        public System.Windows.Forms.CheckBox checkBoxEje3;
        public System.Windows.Forms.CheckBox checkBoxEje4;
        public System.Windows.Forms.CheckBox checkBoxEje5;
        public System.Windows.Forms.CheckBox checkBoxEje1;
        public System.Windows.Forms.Button btnSiguiente;
    }
}