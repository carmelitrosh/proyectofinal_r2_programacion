﻿namespace ProyectoFinal
{
    partial class GestionRutinaJueves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRepeticiones = new MiLibreria.ErrortxtBox();
            this.lblRepeticiones = new System.Windows.Forms.Label();
            this.txtSeries = new MiLibreria.ErrortxtBox();
            this.lblSeries = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.txtNumEjercicio = new MiLibreria.ErrortxtBox();
            this.txtIdEjercicio = new MiLibreria.ErrortxtBox();
            this.txtNombre = new MiLibreria.ErrortxtBox();
            this.lblNumEjercicio = new System.Windows.Forms.Label();
            this.lblNombreEjercicio = new System.Windows.Forms.Label();
            this.lblId_Ejercicio = new System.Windows.Forms.Label();
            this.lblDiaSemana = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(530, 270);
            this.btnGuardar.Size = new System.Drawing.Size(106, 23);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(530, 321);
            this.btnEliminar.Size = new System.Drawing.Size(106, 23);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(530, 370);
            this.btnNuevo.Size = new System.Drawing.Size(106, 23);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(530, 415);
            this.btnSalir.Size = new System.Drawing.Size(106, 23);
            // 
            // txtRepeticiones
            // 
            this.txtRepeticiones.Location = new System.Drawing.Point(278, 418);
            this.txtRepeticiones.Name = "txtRepeticiones";
            this.txtRepeticiones.Size = new System.Drawing.Size(184, 20);
            this.txtRepeticiones.SoloN = false;
            this.txtRepeticiones.TabIndex = 54;
            this.txtRepeticiones.Validar = true;
            // 
            // lblRepeticiones
            // 
            this.lblRepeticiones.AutoSize = true;
            this.lblRepeticiones.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepeticiones.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblRepeticiones.Location = new System.Drawing.Point(186, 420);
            this.lblRepeticiones.Name = "lblRepeticiones";
            this.lblRepeticiones.Size = new System.Drawing.Size(86, 18);
            this.lblRepeticiones.TabIndex = 53;
            this.lblRepeticiones.Text = "Repeticiones";
            // 
            // txtSeries
            // 
            this.txtSeries.Location = new System.Drawing.Point(278, 376);
            this.txtSeries.Name = "txtSeries";
            this.txtSeries.Size = new System.Drawing.Size(184, 20);
            this.txtSeries.SoloN = false;
            this.txtSeries.TabIndex = 52;
            this.txtSeries.Validar = true;
            // 
            // lblSeries
            // 
            this.lblSeries.AutoSize = true;
            this.lblSeries.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeries.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSeries.Location = new System.Drawing.Point(227, 378);
            this.lblSeries.Name = "lblSeries";
            this.lblSeries.Size = new System.Drawing.Size(45, 18);
            this.lblSeries.TabIndex = 51;
            this.lblSeries.Text = "Series";
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ProyectoFinal.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(257, 42);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(230, 171);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 50;
            this.picLogo.TabStop = false;
            // 
            // txtNumEjercicio
            // 
            this.txtNumEjercicio.Location = new System.Drawing.Point(278, 284);
            this.txtNumEjercicio.Name = "txtNumEjercicio";
            this.txtNumEjercicio.Size = new System.Drawing.Size(184, 20);
            this.txtNumEjercicio.SoloN = false;
            this.txtNumEjercicio.TabIndex = 49;
            this.txtNumEjercicio.Validar = true;
            // 
            // txtIdEjercicio
            // 
            this.txtIdEjercicio.Location = new System.Drawing.Point(278, 236);
            this.txtIdEjercicio.Name = "txtIdEjercicio";
            this.txtIdEjercicio.Size = new System.Drawing.Size(184, 20);
            this.txtIdEjercicio.SoloN = true;
            this.txtIdEjercicio.TabIndex = 48;
            this.txtIdEjercicio.Validar = true;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(278, 330);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(184, 20);
            this.txtNombre.SoloN = false;
            this.txtNombre.TabIndex = 47;
            this.txtNombre.Validar = true;
            // 
            // lblNumEjercicio
            // 
            this.lblNumEjercicio.AutoSize = true;
            this.lblNumEjercicio.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumEjercicio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNumEjercicio.Location = new System.Drawing.Point(141, 283);
            this.lblNumEjercicio.Name = "lblNumEjercicio";
            this.lblNumEjercicio.Size = new System.Drawing.Size(132, 18);
            this.lblNumEjercicio.TabIndex = 46;
            this.lblNumEjercicio.Text = "Numero de Ejercicio";
            // 
            // lblNombreEjercicio
            // 
            this.lblNombreEjercicio.AutoSize = true;
            this.lblNombreEjercicio.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreEjercicio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblNombreEjercicio.Location = new System.Drawing.Point(213, 332);
            this.lblNombreEjercicio.Name = "lblNombreEjercicio";
            this.lblNombreEjercicio.Size = new System.Drawing.Size(59, 18);
            this.lblNombreEjercicio.TabIndex = 45;
            this.lblNombreEjercicio.Text = "Nombre";
            // 
            // lblId_Ejercicio
            // 
            this.lblId_Ejercicio.AutoSize = true;
            this.lblId_Ejercicio.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId_Ejercicio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblId_Ejercicio.Location = new System.Drawing.Point(193, 238);
            this.lblId_Ejercicio.Name = "lblId_Ejercicio";
            this.lblId_Ejercicio.Size = new System.Drawing.Size(79, 18);
            this.lblId_Ejercicio.TabIndex = 44;
            this.lblId_Ejercicio.Text = "id_Ejercicio";
            // 
            // lblDiaSemana
            // 
            this.lblDiaSemana.AutoSize = true;
            this.lblDiaSemana.Font = new System.Drawing.Font("Palatino Linotype", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaSemana.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDiaSemana.Location = new System.Drawing.Point(302, 149);
            this.lblDiaSemana.Name = "lblDiaSemana";
            this.lblDiaSemana.Size = new System.Drawing.Size(150, 47);
            this.lblDiaSemana.TabIndex = 55;
            this.lblDiaSemana.Text = "JUEVES";
            // 
            // GestionRutinaJueves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 471);
            this.Controls.Add(this.lblDiaSemana);
            this.Controls.Add(this.txtRepeticiones);
            this.Controls.Add(this.lblRepeticiones);
            this.Controls.Add(this.txtSeries);
            this.Controls.Add(this.lblSeries);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.txtNumEjercicio);
            this.Controls.Add(this.txtIdEjercicio);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNumEjercicio);
            this.Controls.Add(this.lblNombreEjercicio);
            this.Controls.Add(this.lblId_Ejercicio);
            this.Name = "GestionRutinaJueves";
            this.Text = "GestionRutinaJueves";
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnNuevo, 0);
            this.Controls.SetChildIndex(this.lblId_Ejercicio, 0);
            this.Controls.SetChildIndex(this.lblNombreEjercicio, 0);
            this.Controls.SetChildIndex(this.lblNumEjercicio, 0);
            this.Controls.SetChildIndex(this.txtNombre, 0);
            this.Controls.SetChildIndex(this.txtIdEjercicio, 0);
            this.Controls.SetChildIndex(this.txtNumEjercicio, 0);
            this.Controls.SetChildIndex(this.picLogo, 0);
            this.Controls.SetChildIndex(this.lblSeries, 0);
            this.Controls.SetChildIndex(this.txtSeries, 0);
            this.Controls.SetChildIndex(this.lblRepeticiones, 0);
            this.Controls.SetChildIndex(this.txtRepeticiones, 0);
            this.Controls.SetChildIndex(this.lblDiaSemana, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MiLibreria.ErrortxtBox txtRepeticiones;
        private System.Windows.Forms.Label lblRepeticiones;
        private MiLibreria.ErrortxtBox txtSeries;
        private System.Windows.Forms.Label lblSeries;
        private System.Windows.Forms.PictureBox picLogo;
        private MiLibreria.ErrortxtBox txtNumEjercicio;
        private MiLibreria.ErrortxtBox txtIdEjercicio;
        private MiLibreria.ErrortxtBox txtNombre;
        private System.Windows.Forms.Label lblNumEjercicio;
        private System.Windows.Forms.Label lblNombreEjercicio;
        private System.Windows.Forms.Label lblId_Ejercicio;
        private System.Windows.Forms.Label lblDiaSemana;
    }
}