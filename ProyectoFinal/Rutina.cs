﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class Rutina : Gestion
    {
        public Rutina()
        {
            InitializeComponent();
        }

        private void checkBoxLunes_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void btnRutinaDia_Click(object sender, EventArgs e)
        {
            if (checkBoxLunes.Checked == true)
            {
                ReLunes Ru = new ReLunes();
                Ru.Show();
            }
   

            if (checkBoxMartes.Checked == true)
            {
                ReMartes Ru = new ReMartes();
                Ru.Show();
            }


            if (checkBoxMiercoles.Checked == true)
            {
                ReMiercoles Ru = new ReMiercoles();
                Ru.Show();
            }


            if (checkBoxJueves.Checked == true)
            {
                ReJueves Ru = new ReJueves();
                Ru.Show();
            }


            if (checkBoxViernes.Checked == true)
            {
                ReViernes Ru = new ReViernes();
                Ru.Show();
            }


            if (checkBoxLunes.Checked == false && checkBoxMartes.Checked == false && checkBoxMiercoles.Checked == false && checkBoxJueves.Checked == false && checkBoxViernes.Checked == false)
            {
                MessageBox.Show("Escoje un dia por lo menos ");
            }


        }
    }
}
