﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;

namespace ProyectoFinal
{
    public partial class INICIO : FormBase
    {
        public INICIO()
        {
            InitializeComponent();
        }

        public static String Codigo= "";


        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * FROM Usuarios WHERE account ='{0}' AND passw = '{1}'", txtUsuario.Text.Trim(), txtContra.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);

                Codigo = ds.Tables[0].Rows[0]["id_usuario"].ToString().Trim();


                string cuenta = ds.Tables[0].Rows[0]["account"].ToString().Trim();
                string contra = ds.Tables[0].Rows[0]["passw"].ToString().Trim();

                if(cuenta == txtUsuario.Text.Trim() && contra == txtContra.Text.Trim())
                {
                    MessageBox.Show("Se ha iniciado");

                    if(Convert.ToBoolean(ds.Tables[0].Rows[0]["Status_admin"])== true)
                    {
                        VentanaAdmin VenAd = new VentanaAdmin();

                        this.Hide();
                        VenAd.Show();
                    }
                    else
                    {
                        VentanaUser VenAd = new VentanaUser();

                        this.Hide();
                        VenAd.Show();
                    }
                }


            }

            catch (Exception error)
            {
                MessageBox.Show("Usuario o contraseña incorrecta");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void INICIO_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
